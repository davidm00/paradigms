# client-server-final-project

DATA:
    
    - Stock retrieval - https://www.alphavantage.co/documentation/
    - API Key - WZ61LSDWRKDVLGOD
 
Our own ticker data - TICKER.dat
    
    - TICKERS
        - TICKER (IBM, FB, NFLX, etc…)
            - ID
            - Symbol
            - Category


**********************
**JSON SPECIFICATION**
**********************


**PUT_UPDATE_PASSWORD**

    - Update password

**PUT_TICKER**
    
    - Add new ticker to TICKER.dat if it doesn't exist already

**DELETE_TICKER**
    
    - Remove ticker if user id matches

**GET_TICKERS**
    
    - Retrieve ticker from TICKER.dat by type
