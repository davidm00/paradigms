import unittest
import requests
import json

class TestTickerEndpoint(unittest.TestCase):

    SITE_URL = 'http://student00.cse.nd.edu:51033' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    TICKERS_URL = SITE_URL + '/ticker/'
    RESET_URL = SITE_URL + '/treset/'

    def reset_data(self):
        t = {}
        r = requests.put(self.RESET_URL, json.dumps(t))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_ticker_get(self):
        self.reset_data()
        ticker_id = 1
        r = requests.get(self.TICKERS_URL + str(ticker_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        
        self.assertEqual(resp['symbol'], 'ALRM')
        self.assertEqual(resp['category'], 'Technology')

    def test_tickers_get(self):
        self.reset_data()
        r = requests.get(self.TICKERS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        testticker = {}
        tickers = resp['tickers']
        for ticker in tickers:
            if ticker['id'] == 2:
                testticker = ticker

        self.assertEqual(testticker['symbol'], 'BABA')
        self.assertEqual(testticker['category'], 'Technology')

    def test_ticker_post(self):
        self.reset_data()

        t = {}
        t['symbol'] = 'FAKE'
        t['category'] = 'Technology'
        r = requests.post(self.TICKERS_URL, data = json.dumps(t))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], 41)

        r = requests.get(self.TICKERS_URL + str(resp['id']))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['symbol'], t['symbol'])
        self.assertEqual(resp['category'], t['category'])
        # for elem in resp['tickers']:
        #     if elem['id'] == 41:
        #         self.assertEqual(elem['symbol'], t['symbol'])
        #         self.assertEqual(elem['category'], t['category'])

    def test_ticker_delete(self):
        self.reset_data()

        t = {}
        r = requests.delete(self.TICKERS_URL + str(2), data = json.dumps(t))
        test = r.content.decode('utf-8')

        self.assertTrue(self.is_json(test))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.TICKERS_URL + str(2))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'error')
        

if __name__ == "__main__":
    unittest.main()

