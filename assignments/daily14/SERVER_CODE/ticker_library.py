#!/usr/bin/env python3

class _ticker_database:
       def __init__(self):
            self.ticker_symbol = dict()
            self.ticker_category = dict()

       def load_tickers(self, data):
        f = open(data)
        for line in f:
            line = line.rstrip()
            components = line.split("::")
            tid = int(components[0])
            tsymbol = components[1]
            tcategory = components[2]
            self.ticker_symbol[tid] = tsymbol
            self.ticker_category[tid] = tcategory
        f.close()

       def get_tickers(self):
            return self.ticker_symbol.keys()

       def get_ticker(self, tid):
        try:
            tsymbol = self.ticker_symbol[tid]
            tcategory = self.ticker_category[tid]
            ticker = list((tsymbol, tcategory))
        except Exception as ex:
            ticker = None
        return ticker

       def set_ticker(self, tid, ticker):
            self.ticker_symbol[tid] = ticker[0]
            self.ticker_category[tid] = ticker[1]

       def delete_ticker(self, tid):
            del(self.ticker_symbol[tid])
            del(self.ticker_category[tid])

if __name__ == "__main__":
       tdb = _ticker_database()

       #### TICKERS ########
       tdb.load_tickers('tickers.dat')

       ticker = tdb.get_ticker(2)
       print(ticker[0])

       ticker[0] = 'ABC'
       tdb.set_ticker(2, ticker)

       ticker = tdb.get_ticker(2)
       print(ticker[0])
       ####################
