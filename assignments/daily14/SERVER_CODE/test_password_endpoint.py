import unittest
import requests
import json

class TestPassword(unittest.TestCase):
	SITE_URL = 'http://student00.cse.nd.edu:51033'
	print('testing for server: ' + SITE_URL)
	PASSWORD_URL = SITE_URL + '/password/'
	RESET_URL = SITE_URL + '/preset/'

	def reset_data(self):
		p = {}
		r = requests.put(self.RESET_URL + str(1), data = json.dumps(p))

	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_password_get_key(self):
		self.reset_data()
		password_id = 1

		r = requests.get(self.PASSWORD_URL + str(password_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['password'], 'dogs123')

	def test_password_put_key(self):
		self.reset_data()
		password_id = 1
		p = {}
		p['password'] = 'newPassword'

		r = requests.put(self.PASSWORD_URL + str(1), data=json.dumps(p))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))

		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.PASSWORD_URL + str(password_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['password'][0], p['password'])

if __name__ == '__main__':
	unittest.main()


