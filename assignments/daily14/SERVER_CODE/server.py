import cherrypy
from tickerController import TickerController
from ticker_library import _ticker_database
from password_library import _password_database
from passwordController import PasswordController
from resetTickerController import ResetTickerController
from resetPasswordController import ResetPasswordController

class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	tdb = _ticker_database()
	pdb = _password_database()

	tickerController 		= TickerController(tdb=tdb)
	resetTickerController 	= ResetTickerController(tdb=tdb)
	passwordController 		= PasswordController(pdb=pdb)
	resetPasswordController = ResetPasswordController(pdb=pdb)

	

	# CORS RELATED CONNECTIONS
	dispatcher.connect('password_put_options', '/password/:password_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('ticker_post', '/ticker/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('ticker_delete_options', '/ticker/:ticker_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	# dispatcher.connect('tickers_get', '/ticker/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	# dispatcher.connect('ticker_get', '/ticker/:ticker_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('reset_index_put', '/treset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('reset_put', '/preset/:password_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

	# DEFAULT CONNECTIONS
	dispatcher.connect('reset_key_put', '/treset/:ticker_id', controller=resetTickerController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_index_put', '/treset/', controller=resetTickerController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

	dispatcher.connect('reset_put', '/preset/:password_id', controller=resetPasswordController, action = 'PUT_KEY', conditions=dict(method=['PUT']))

	dispatcher.connect('password_put', '/password/:password_id', controller=passwordController, action = 'PUT_UPDATE_PASSWORD', conditions=dict(method=['PUT']))
	dispatcher.connect('password_get', '/password/:password_id', controller=passwordController, action = 'GET_PASSWORD', conditions=dict(method=['GET']))

	dispatcher.connect('ticker_post', '/ticker/', controller=tickerController, action = 'POST_TICKER', conditions=dict(method=['POST']))
	dispatcher.connect('ticker_delete', '/ticker/:ticker_id', controller=tickerController, action = 'DELETE_TICKER', conditions=dict(method=['DELETE']))
	dispatcher.connect('tickers_get', '/ticker/', controller=tickerController, action = 'GET_TICKERS', conditions=dict(method=['GET']))
	dispatcher.connect('ticker_get', '/ticker/:ticker_id', controller=tickerController, action = 'GET_TICKER', conditions=dict(method=['GET']))


	conf = {
			'global': {
				'server.thread_pool': 5,
				'server.socket_host': 'student04.cse.nd.edu',
				'server.socket_port': 51033,
				},
			'/': {
				'request.dispatch': dispatcher,
				'tools.CORS.on': True,
				}
			}

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config = conf)
	cherrypy.quickstart(app)

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) #turn cors tool on
	start_service()

