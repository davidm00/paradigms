#!/usr/bin/env python3

import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student00.cse.nd.edu:51033' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/treset/'
    TICKERS_URL = SITE_URL + '/ticker/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        t = {}
        r = requests.put(self.RESET_URL)

        t['symbol'] = 'FAKE'
        t['category'] = 'Technology'
        r = requests.put(self.RESET_URL, data=json.dumps(t))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        r = requests.put(self.RESET_URL, data=json.dumps(t))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.TICKERS_URL + str(2))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['symbol'], 'BABA')

    def test_put_reset_key(self):
        t = {}
        r = requests.put(self.RESET_URL)

        t['symbol'] = 'FAKE'
        t['category'] = 'Technology'

        r = requests.post(self.TICKERS_URL, data=json.dumps(t))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.put(self.RESET_URL, data=json.dumps(t))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.TICKERS_URL + str(41))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()