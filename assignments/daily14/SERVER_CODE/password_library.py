#!/usr/bin/env python3

class _password_database():
	def __init__(self):
		self.password = dict()
	
	def load_password(self, data):
		f = open(data)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			pid = int(components[0])
			password = components[1]
			self.password[pid] = password
		f.close()

	def get_passwords(self):
		return self.password.keys()

	def get_password(self, pid):
		try:
			password = self.password[pid]
		except Exception as ex:
			password = None
		return password

	def set_password(self, pid, password):
		self.password[pid] = password

	def delete_password(self, pid):
		del(self.password[pid])

if __name__ == "__main__":
	pdb = _password_database()
	
	#### PASSWORD ########
	pdb.load_password('password.dat')

	password = pdb.get_password(1)
	print(password)

	passowrd = 'ABC'
	pdb.set_password(2, passowrd)

	password = pdb.get_password(2)
	print(password)

	pdb.delete_password(2)
	print(pdb.get_password(2))

