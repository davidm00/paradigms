#!/usr/bin/env python3

import cherrypy
import re, json
from ticker_library import _ticker_database

class ResetTickerController(object):

    def __init__(self, tdb=None):
        if tdb is None:
            self.tdb = _ticker_database()
        else:
            self.tdb = tdb


    def PUT_INDEX(self):
        output = {'result':'success'}

        try:
            data = json.loads(cherrypy.request.body.read().decode())
            self.tdb.__init__()
            self.tdb.load_tickers('tickers.dat')
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, ticker_id):
        output = {'result':'success'}
        tid = int(ticker_id)

        try:
            data = json.loads(cherrypy.request.body.read().decode())

            tdbtmp = _ticker_database()
            tdbtmp.load_tickers('tickers.dat')

            ticker = tdbtmp.get_ticker(tid)
            
            self.tdb.set_ticker(tid, ticker)


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

