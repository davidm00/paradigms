#!/usr/bin/env python3

import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student00.cse.nd.edu:51033' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/preset/'
    PASSWORD_URL = SITE_URL + '/password/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_key(self):
        p = {}
        r = requests.put(self.RESET_URL + str(1))

        p['password'] = 'NEWPASSWORDLOL'

        r = requests.put(self.PASSWORD_URL + str(1), data=json.dumps(p))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.put(self.RESET_URL + str(1), data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PASSWORD_URL + str(1))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['password'], 'dogs123')

if __name__ == "__main__":
    unittest.main()