#!/usr/bin/env python3

import cherrypy
import re, json
from password_library import _password_database

class ResetPasswordController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _password_database()
        else:
            self.pdb = pdb

    def PUT_KEY(self, password_id):
        output = {'result':'success'}
        pid = int(password_id)

        try:
            data = json.loads(cherrypy.request.body.read().decode())

            pdbtmp = _password_database()
            pdbtmp.load_password('password.dat')

            password = pdbtmp.get_password(pid)
            
            self.pdb.set_password(pid, password)


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

