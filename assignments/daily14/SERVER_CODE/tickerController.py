#!/usr/bin/env python3

import cherrypy
import re, json
from ticker_library import _ticker_database

class TickerController(object):

    def __init__(self, tdb=None):
            if tdb is None:
                self.tdb = _tikcer_database()
            else:
                self.tdb = tdb

            self.tdb.load_tickers('tickers.dat')

    def POST_TICKER(self):
        output = {'result' : 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
            
        
        ticker = list()
        ticker.append(data['symbol'])
        ticker.append(data['category'])
        tids = self.tdb.get_tickers()
        
        try:
            tid = max(tids) + 1
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        self.tdb.set_ticker(tid, ticker)
        output['id'] = tid

        return json.dumps(output)

    def DELETE_TICKER(self, ticker_id):
        output = {'result' : 'success'}
        ticker_id = int(ticker_id)

        
        print("ticker_id: " + str(ticker_id))
            
        try:
            print("SUCCESS")
            self.tdb.delete_ticker(ticker_id)
        except Exception as ex:
            print("FAILED")
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_TICKERS(self):
        output = {'result':'success'}
        output['tickers'] = []

        try:
            for tid in self.tdb.get_tickers():
                ticker = self.tdb.get_ticker(tid)
                dticker = {'id':tid, 'symbol':ticker[0], 'category':ticker[1]}
                output['tickers'].append(dticker)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_TICKER(self, ticker_id):
        output = {'result':'success'}
        ticker_id = int(ticker_id)

        try:
            ticker = self.tdb.get_ticker(ticker_id)
            if ticker is not None:
                output['id'] = ticker_id
                output['symbol'] = ticker[0]
                output['category'] = ticker[1]
            else:
                output['result'] = 'error'
                output['message'] = 'ticker not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


        