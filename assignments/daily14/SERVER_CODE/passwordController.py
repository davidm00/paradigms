import cherrypy
import re, json
from password_library import _password_database

class PasswordController(object):

	def __init__(self, pdb=None):
		if pdb is None:
			self.pdb = _password_database()
		else:
			self.pdb = pdb

		self.pdb.load_password('password.dat')
		print("INITIALIZED DATA")

	def PUT_UPDATE_PASSWORD(self, password_id):
		output = {'result':'success'}
		password_id = int(password_id)

		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		password = list()
		password.append(data['password'])

		self.pdb.set_password(password_id, password)

		return json.dumps(output)

	def GET_PASSWORD(self, password_id):
		output = {'result' : 'success'}
		password_id = int(password_id)

		try:
			password = self.pdb.get_password(password_id)
			if password is not None:
				output['password'] = password
			else:
				output['result'] = 'error'
				output['message'] = 'password not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
