console.log('page load - entered main.js for js-other api');

send_button = document.getElementById("send-button");
send_button.onmouseup = makeRequest;

function makeRequest(){
    var selindex = document.getElementById('select-server-address').selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selindex].value;
    var port_num = document.getElementById('input-port-number').value;
    var action = "GET"; // default
    var endpoint = "/ticker/"
    console.log('entered make nw call');
    var key = null;
    if (document.getElementById('checkbox-use-key').checked) {
        key = document.getElementById('input-key').value;
    }
    if (document.getElementById('radio-get').checked){
        action = "GET";
        endpoint = "/ticker/"
    } else if (document.getElementById('radio-put').checked) {
            action = "PUT";
            endpoint = "/password/"
    } else if (document.getElementById('radio-post').checked) {
            action = "POST"
            endpoint = "/ticker/"
    } else if (document.getElementById('radio-delete').checked){
            action = "DELETE"
            endpoint = "/ticker/"
    }
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    
    if (action == "GET"){
        if(key != null){
            // console.log("Key: " + key);
            var url = url_base + ":" + port_num + endpoint + key;
        }else{
            // console.log("Key: " + key);
            var url = url_base + ":" + port_num + endpoint;
        }
        console.log("GET----URL: " + url)
    } else if (action == "PUT"){
        key = document.getElementById('input-key').value;
        var url = url_base + ":" + port_num + endpoint + key;
        console.log("PUT----URL: " + url)
    } else if (action == "POST"){
        var url = url_base + ":" + port_num + endpoint;
        console.log("POST----URL: " + url)
    } else if (action == "DELETE"){
        key = document.getElementById('input-key').value
        var url = url_base + ":" + port_num + endpoint + key;
        console.log("DELETE----URL: " + url)
    }


    xhr.open(action, url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        format(action, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }
    console.log("Url: " + url)
    // xhr.send(null)
    var body = null;
    // actually make the network call
    if (action == "GET"){
        xhr.send(body)
    } else if (action == "PUT"){
        body = document.getElementById('text-message-body').value;
        xhr.send(body)
    } else if (action == "POST"){
        body = document.getElementById('text-message-body').value;
        xhr.send(body)
    } else if (action == "DELETE"){
        xhr.send(body)
    }
}

function format(action, response){
    var response_json = JSON.parse(response);
    // update a label
    var jso = document.getElementById("answer-label");
    jso.innerHTML = response;
}