console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    // var name = document.getElementById("name-text").value;
    console.log('Debored-ify Begin');
    makeNetworkCallToIPApi();

} // end of get form info

function makeNetworkCallToIPApi(){
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.ipify.org?format=json";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        findMyIP(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function findMyIP(response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json['ip'] == null){
        label1.innerHTML = 'Apologies, we could not find your IP Address.';
    } else{
        label1.innerHTML = 'Here\'s your ip address: ' + response_json["ip"];
        makeNetworkCallToIPCountry(response_json["ip"])
    }
} // end of updateAgeWithResponse

function makeNetworkCallToIPCountry(ip){
    console.log('entered make nw call: ' + ip);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://freegeoip.app/json/" + ip;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateDOMWithResponse(ip, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateDOMWithResponse(ip, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = "According to your ip address, " + ip + ", you are in " + response_json["city"] + ", " + response_json["region_code"];
    var label3 = document.getElementById("response-line3");
    label3.innerHTML = "Your coordinates --- Latitude: " + response_json["latitude"] + ", Longitude: " + response_json["longitude"] + ".";
    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object
    label_item.innerHTML = response_json["country_code"] + "...";
    // var item_text = document.createTextNode(response_json["countryCode"]); // creating new text
    // label_item.appendChild(item_text); // adding something to button with appendChild()

    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
