// respond to button click
console.log("Page load happened!")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from title, artist and lyrics
    var title_text = document.getElementById('title-text').value;
    var artist_text = document.getElementById('artist-text').value;
    var lyrics_text = document.getElementById('text-lyrics').value;
    console.log('title:' + title_text + ' artist: ' + artist_text + ' lyrics ' + lyrics_text);

    // get checkbox state
    var genres_string = "";
    if (document.getElementById('checkbox-rap-value').checked){
        console.log('detected rap!');
        genres_string += "Rap,";
    }

    if (document.getElementById('checkbox-r&b-value').checked) {
        console.log('detected r&b!');
        genres_string += "R&B,";
    }

    if (document.getElementById('checkbox-house-value').checked) {
        console.log('detected house!');
        genres_string += "House";
    }
    // make genre combined string
    console.log('genres: ' + genres_string);

    // make dictionary
    song_dict = {};
    song_dict['title'] = title_text;
    song_dict['artist'] = artist_text;
    song_dict['lyrics'] = lyrics_text;
    song_dict['genres'] = genres_string;
    console.log(song_dict);

    displaySong(song_dict);

}

function displaySong(song_dict){
    console.log('entered displaySong!');
    console.log(song_dict);
    // get fields from song and display in label.
    var song_top = document.getElementById('song-top-line');
    song_top.innerHTML = song_dict['title'] + ' by ' + song_dict['artist'];

    var song_body = document.getElementById('song-body');
    song_body.innerHTML = song_dict['lyrics'];

}
